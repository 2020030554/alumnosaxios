const express = require('express')
const router = express.Router()
const ejs = require('ejs');
const bodyparser = require('body-parser')
const db = require('../models/alumno.js')
const axios = require('axios')  

let alumno = {
    Matricula: "",
    Nombre: "",
    Domicilio: "",
    Sexo: "",
    Especialidad: ""
}

let campos = {
    Matricula: "",
    Nombre: "",
    Domicilio: "",
    Sexo: "",
    Especialidad: ""
}



//Hecho en clase
router.post('/insertar',async(req,res)=>{
    
    alumno = {
        Matricula: req.body.Matricula,
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Sexo: req.body.Sexo,
        Especialidad: req.body.Especialidad
    }   

    let resultado = await db.insertar(alumno)
    res.json(resultado)
})

router.get('/consultar',async(req,res)=>{

    let resultado = await db.consultar()
    res.json(resultado)
})

router.post('/consultarMatricula',async(req,res)=>{

    Matricula = req.body.Matricula
    let resultado = await db.consultarMatricula(Matricula)
    res.json(resultado)
})

router.post('/eliminar',async(req,res)=>{

    Matricula = req.body.Matricula
    let resultado = await db.eliminar(Matricula)
    res.json(resultado)
})

router.post('/actualizar',async(req,res)=>{

    alumno = {
        Matricula: req.body.Matricula,
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Sexo: req.body.Sexo,
        Especialidad: req.body.Especialidad
    }   
    let resultado = await db.actualizar(alumno)
    res.json(resultado)
})














//Axios
router.get('/',(req,res)=>{

    axios({
        method: 'get',
        url: 'http://localhost:3001/consultar',
    })
        .then(function (response) {
            res.render('index.html',{titulo: "Listado de alumnos", listado: response.data, campos:campos})
        });
})

router.post('/consultarPorMatricula',async (req,res)=>{
    Matricula = req.body.Matricula
    let resultado = await db.consultarMatricula(Matricula)
    let lista = await db.consultar()
    res.render('index.html',{titulo: "Listado de alumnos", listado: lista, campos:resultado})
    
})

router.get('/agregar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:3001/insertar',
        data: {
            Matricula: req.query.Matricula,
            Nombre: req.query.Nombre,
            Domicilio: req.query.Domicilio,
            Sexo: req.query.Sexo,
            Especialidad: req.query.Especialidad
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})

router.get('/borrar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:3001/eliminar',
        data: {
            Matricula: req.query.Matricula
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})
router.get('/modificar',(req,res)=>{

    axios({
        method: 'post',
        url: 'http://localhost:3001/actualizar',
        data: {
            Matricula: req.query.Matricula,
            Nombre: req.query.Nombre,
            Domicilio: req.query.Domicilio,
            Sexo: req.query.Sexo,
            Especialidad: req.query.Especialidad
        }
    })
        .then(function (response) {
            res.redirect('/')
        });
})









// Declarar datos
let datos = [{
    matricula:"2020030324",
    nombre: "Víctor Uriel Juárez Lugo",
    sexo: "H",
    materias:["Inglés", " Tecnologías de internet", " Base de datos"]
},
{
    matricula:"2020030325",
    nombre: "Víctoria Juárez López",
    sexo: "M",
    materias:["Español", " Matemáticas", " Base de datos"]

},
{
    matricula:"2020030312",
    nombre: "Lizbet Argelia Padilla Moreno",
    sexo: "M",
    materias:["Diseño de interfaces", " Inteligencia artificial", " Base de datos"]
},
{
    matricula:"2020030326",
    nombre: "Pedro Antonio Sanchez Salas",
    sexo: "H",
    materias:["Inglés", " Programación", " Diseño web"]
},
{
    matricula:"2020030327",
    nombre: "Brian Eduardo Ríos Muñoz",
    sexo: "H",
    materias:["Desarrollo web", " Matemáticas", " Inglés"]
},
{
    matricula:"2020030328",
    nombre: "Carlos alberto García Medrano",
    sexo: "H",
    materias:["Geografía", " Matemáticas", " Base de datos"]
}
]
router.get('/', (req, res)=>{
    //res.send("<h1>Iniciamos con express</h1>");
    res.render('index.html',{titulo: "Lista de alumnos", listado:datos})
    
    
})

// Tablas
router.get("/tablas", (req, res)=>{
    const valores = {
        tabla:req.query.tabla
    }
    res.render('tablas.html', valores);

})

router.post("/tablas", (req, res)=>{
    const valores = {
        tabla:req.body.tabla
    }
    res.render("tablas.html", valores);
})


// Cotización
router.get('/cotizacion', (req, res) => {
    const resultados = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
       
    }
    
    res.render('cotizacion.html', resultados);
  });

router.post('/cotizacion', (req, res)=>{
    const resultados = {
        
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion.html', resultados);
})
  


module.exports = router;